package Tema5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredDataGUI {

	String fileName = new String("Activities.txt");
	String writeFile = new String("Results.txt");
	List<String> list = new ArrayList<String>();
	List<MonitoredData> monitoredList = new ArrayList<MonitoredData>();

	public MonitoredDataGUI() throws IOException {
		readStream();
		System.out.println(zileMonitorizate());
		System.out.println(actiuniDistincte());
		System.out.println(actiuniDistincteZi());
		System.out.println(getDurata());
		writeStream();
	}

	public void writeStream() throws FileNotFoundException, UnsupportedEncodingException {

		PrintWriter writer = new PrintWriter(writeFile, "UTF-8");
		writer.println(zileMonitorizate());
		writer.println(actiuniDistincte());
		writer.println(actiuniDistincteZi());
		writer.println(getDurata());
		writer.close();
	}

	public MonitoredData splitStream(String string) {
		String[] split1 = string.split("		", 2);
		String[] split2 = split1[1].split("		", 2);
		String[] split3 = split2[1].split("		", 2);
		String start = split1[0];
		String stop = split2[0];
		String activity = split3[0];	
		MonitoredData MD = new MonitoredData(start, stop, activity);
		return MD;
	}

	public long zileMonitorizate() {
		long k = monitoredList.stream().map(temp -> temp.getStartDay()).distinct().count();
		//		{
		//			String[] compare = temp.getStartTime().split(" ");
		//			return compare[0];
		//		}).distinct().count();
		return k;
	}

	/*	public Map <String, Integer> countTimes2() { 
		Map <String, Integer> activity = new HashMap<String, Integer>();	
		monitoredList.stream().map(temp ->
		{
			activity.put(temp.getActivity(), 0);
			return temp.getActivity();
		}).distinct().count();
		monitoredList.stream().map(temp-> {
			int u = activity.get(temp.getActivity());
			u++;
			activity.remove(activity.get(temp.getActivity()), u-1);
			activity.put(temp.getActivity(), u);
			return null;
		}).count();
		return activity;
	}*/

	public Map<String, Long> actiuniDistincte() {
		Map<String, Long> activity =
				monitoredList.stream().collect(
						Collectors.groupingBy(
								e -> e.getActivity(), Collectors.counting()
								)
						);
//		System.out.println(activity);
		return activity;
	}

	public Map<String, Map<String, Long>> actiuniDistincteZi() {
		Map<String, Map<String, Long>> activity = 
				monitoredList.stream().collect(
						Collectors.groupingBy(
								e -> e.getStartDay(), Collectors.groupingBy(
										e->e.getActivity(), Collectors.counting()
										)
								)
						);
		//		System.out.println(activity);
		return activity;
	}

	public Map<String, Long> getDurata(){
		Map<String, Long> activity = 
				monitoredList.stream().collect(
						Collectors.groupingBy(
								e->e.getActivity(), Collectors.summingLong(e->{
									try {
//						System.out.println(Math.abs(e.getEndDate().getTime()/1000-e.getStartDate().getTime()/1000));
											//if(Math.abs(e.getEndDate().getTime()/1000-e.getStartDate().getTime()/1000) > 36000)
										return Math.abs(e.getEndDate().getTime()/1000-e.getStartDate().getTime()/1000);
									} catch (ParseException e1) {
										e1.printStackTrace();
									}
									return 0;
								})));
		return activity;
	}

	public void readStream() throws IOException{
		Stream<String> stream = Files.lines(Paths.get(fileName));
		stream.forEach(a -> list.add(a));
		list.stream().map(temp -> {
			MonitoredData MD = new MonitoredData();
			MD = splitStream(temp.toString());
			monitoredList.add(MD);
			//System.out.println(MD.toString());
			return MD;
		}).collect(Collectors.toList());
		//monitoredList.forEach(System.out::println);
		stream.close();
	}

	public static void main (String[] args) throws IOException {
		MonitoredDataGUI tema5 = new MonitoredDataGUI();
	}
}
