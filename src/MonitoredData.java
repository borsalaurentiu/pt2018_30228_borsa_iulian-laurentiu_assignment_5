package Tema5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mysql.fabric.xmlrpc.base.Data;

public class MonitoredData {
	private String start;
	private String end;
	private String activity;
	
	
	public MonitoredData(String start, String end, String activity){
		this.start = start;
		this.end = end;
		this.activity = activity;
		
	}

	public MonitoredData(){		
	}

	public String getstart() {
		return start;
	}
	
	public void setstart(String start) {
		this.start = start;
	}
	
	public String getend() {
		return end;
	}
	
	public void setend(String end) {
		this.end = end;
	}
	
	public String getActivity() {
		return activity;
	}
	
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	@Override
	public String toString() {
		return "Start: " + start + ", stop: " + end + ", activity: " + activity + "\n";
	}

	public String getStartDay() {
		String[] date = start.split(" ", 2);
		return date[0];
	}
	
	public String getEndDay() {
		String[] date = end.split(" ", 2);
		return date[0];
	}
	
	public Date getStartDate() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = formatter.parse(start);
		return date;
	}
	
	public Date getEndDate() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = formatter.parse(end);
		return date;
	}
	
//	public Date interval() throws ParseException {
//		Date date = new Date();
//		date.setTime(Math.abs(getEndDate().getTime() - getStartDate().getTime()));
//		return date;
//	}
	
}
